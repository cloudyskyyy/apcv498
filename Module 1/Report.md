# Project White Paper template

<!-- The bullet points in this document are items you should address **in your narrative**, NOT bullet points to be copied, pasted, and responded to!

In this document I provide an outline based on which you need to include all required items in your project status report. Note that the same set of items are also required in your project proposal. You also need to check the project status report rubric on D2L web site so that you may understand how I will grade your status report. -->

## Heading: Title, Team Name, Team Members

*You should copy the heading from your project proposal.*

## Section 1: Introduction

In your introduction section, you should write down:

* Highlights of what was accomplished
* Overview of changes of requirements, design, and major events.

## Section 2: Customer Value

In this section, you should write down the changes, including changes from project proposal such as:

* Date of change
* Motivation for the change
* Description of the change, including implications.

If no changes, you can simply say "No changes".

## Section 3: Technology

In this section, you should write down:

* The current model of your system on development or design
* What are the goals in this month?
* What works? Include evidence that demonstrates your points.
* What are the goals for next month?
* Also keep track of the changes to design, code, ... for next month iteration

## Section 4: Your Team

In this section, you should write down skills and roles for team members as well as communication methods, including

* What are the roles of the team members during this month iteration?
* Will the roles be fixed or rotating?
* What communication methods your team used and would continually use?
* How do you evaluate the team communication and collaboration?

## Section 5: Project Management

In this section, you should check if you are on schedule:

* Is your team on schedule?
* What is the plan for the rest of the spring term?

## Section 6: Reflection

You are recommended to keep a log so you can also do this section for later reports. In your reflection, you should write down:

* What have been accomplished?
* What went well?
* What didn't go well?
* What will you do differently for the next month iteration?

*The entire body text of the report should be Times New Roman, font 12 pts, and single-line space.*
