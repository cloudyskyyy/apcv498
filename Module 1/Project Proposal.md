﻿# Project Proposal

In this document, I provide an outline based on which you need to include all required items in your project proposal. The intent of the project proposal is to evolve it into your comprehensive nal report as the spring term goes. You also need to check the project proposal rubric on D2L web site so that you may understand how I will grade your proposal.

1. Heading: Title, Team Name, Team Members

    You should write the heading of the project proposal including

    - a descriptive title for the project. The title will be meaningful on your resume.
    - team name. It is your choice. But pick something fun, or professional, or both.
    - team number. To be assigned.
    - team members.

2. Section 1: Introduction

    In your introduction section, you should provide an overview of the proposed project, including:

    - What is the project?
    - What is the motivation for the project?
    - What are the backgrounds of the team members?
    - Is there anything you'd like to include to orient the reader?
3. Section 2: Customer Value

    This is probably the most important section of the proposal. You should write down your customer need and proposed solution:

    - Who is the primary customer?
    - What does the customer want?
    - Why? What is their underlying problem to solve or experience to have?
    - How will the customer benet from your proposed solution?
    - Does it provide a new capability or is it much better, easier, faster ...?
    - Have you tested the idea on anyone?
    - How will you know if customers got the benets you want to provide?
    - What are your customer-centric measures of success?

4. Section 3: Technology

    There are two forms of technology: system and tools. For system to deliver, you should write down:

    - What are the main components of your system? What do they do?
    - What is a high-level representation that describes your system?
    - What is the minimal system that would have some value to the customer?
    - What are some possible enhancements that customers would value?
    - How will you test your system?

    For tools you would use to build what you deliver, you should write down:

    - What will you use to build the system?
    - Are there available tools you can leverage?

5. Section 4: Team

    In this section, you should write down skills and roles for team members as well as communication methods, including

    - Has anyone on the team built something like this before?
    - Are the tools known or new to the team?
    - What are the roles of the team members?
    - Will the roles be xed or rotating?
    - What communication methods your team used and would continually use?
    - How do you evaluate the team communication and collaboration?

6. Section 5: Project Management

    In this section, you should set up project schedule, identify constraints and resources, including

    - Is completion of the system feasible?
    - When and how often will you meet? Face to face?
    - Are there any regulatory, or legal constraints?
    - Will you have access to the resources you need?

7. Section 6: Reection

    You are recommended to keep a log so you can also do this section for later reports. in your reection, you should write down:

    - What have been accomplished?
    - What went well?
    - What didn't go well?
    - Recommendations

    The entire body text of the report should be Times New Roman, font 12 pts, and single-line space.
