# APCV 498 - Senior Capstone

## Course Objectives

These are the activities and subjects that will be covered in this course.

- Formulate problems and describe problem solutions in a chosen specialization.
- Select and use of appropriate process models and/or programming environments.
- Apply knowledge and design hardware and/or software to meet requirements.
- Work effectively in teams to design and implement solutions to computational problems.
- Communicate effectively, both orally and in writing.
- Think critically and creatively, both independently and with others.
- Be aware of project management and business practices.

## Course Outcomes

These are the skills, knowledge and abilities you will have after successfully completing this course.

1. Identify the type of project that they need to do to solve a problem.
2. Working in teams and delegate responsibilities.
3. Identify roles in the team.
4. Communication between their peers.
5. Thinking critically and creatively to solve a problem.
6. Identify their tools and methodologies to solve a problem.
